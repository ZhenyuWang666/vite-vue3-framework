# vue3-element-admin

简体中文 | [English](./README.en.md)

#### 介绍
一个基于Vite+Vue3+Vue Router+Vuex+TS+Element3+axios+Jest+Cypress的后台管理系统

#### 技术栈
Vite+Vue3+Vue Router+Vuex+TS+Element3+axios+Jest+Cypress

#### 安装

yarn  
yarn dev

#### 贡献

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request

