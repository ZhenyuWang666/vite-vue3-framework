# vue3-element-admin

[简体中文](./README.md) | English

#### Description
A background management system based on Vite+Vue3+Vue Router+Vuex+TS+Element3+axios+Jest+Cypress

#### Software Architecture
Vite+Vue3+Vue Router+Vuex+TS+Element3+axios+Jest+Cypress

#### Installation

yarn  
yarn dev

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request

